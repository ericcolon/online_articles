# Basketball crunch time
This article is intended to demonstrate comparative analysis between two datasets (or two parts of one dataset) for insights.

I use basketball data from the 2018-2019 NBA season as the example.

## Data
- srcdata/league_hexbin_stats.pickle are pickled dictionary files, primarily as produced by
matplotlib.hexbin.
- srcdata/crunch_hexbin_stats.pickle are pickled dictionary files, primarily as produced by
matplotlib.hexbin. (of 'crunch time' only)
- srcdata/2018_19_NBA_shot_logs.csv includes raw data of shots including

### Article
Link: TBC